import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from "react-native";

export default function App() {
  const [number, setNumber] = useState('');

  const changeNumber = value => {
    setNumber(value);
  };

  const addNum = () => {
    const rows = [];
    const nums = [['1', '2', '3'], ['4', '5', '6'], ['7', '8', '9'],['.', '0', '=']];
    for (let i=0; i<4; i++){
      let row = [];
      for(let j=0; j<3; j++){
        row.push(
            <TouchableOpacity
                style={styles.button}
            >
              <Text>{nums[i][j]}</Text>
            </TouchableOpacity>
        )
      }
      rows.push(<View>{row}</View>)
    }
    return rows;
  }

  return (
    <View style={styles.container}>
      <View style={styles.numbers}>
        <Text>{number}</Text>
      </View>
      <View style={styles.btnBlock}>
        {() => addNum}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  numbers: {
    borderBottomWidth: 3,
    height: "20%",
    marginTop: 15,
    width: "100%",
    backgroundColor: "#c99a9a"
  },
  button: {
    width: "24.7%",
    backgroundColor: "#d99a4d",
    marginRight: 2,
    marginBottom: 2,
    alignItems: 'center',
  },
  btnBlock: {
    flex: 1,
    flexDirection: "row",
    width: "100%",
    flexWrap: "wrap",
  },
});
