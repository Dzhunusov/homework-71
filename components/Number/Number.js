import React from 'react';
import {TouchableOpacity} from "react-native";
import {StyleSheet} from 'react-native';

const Number = props => {
  return (
      <TouchableOpacity style={styles.button}>
        {props.number}
      </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: "20%",
    height: "25%",
    backgroundColor: "#d99a4d",
  },
})

export default Number;